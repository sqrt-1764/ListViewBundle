Liste = function (tableId) {
    /**
     * Speichern der SearchModes für die einzelnen Spalten
     * 
     * @param json modes
     * @returns {Liste}
     */
    this.setSearchModes = function (searchModes) {
        this.searchModes = jQuery.parseJSON(searchModes);
        return this;
    };

    /**
     * Optionales Setzen eines Callbacks, der im Fehlerfalle aufgerufen wird.
     * Tag nochmal separat übergeben, damit ggf. bequemer geloggt werden kann.
     * 
     * @param {type} callback
     * @returns {Liste}
     */
    this.setErrorCallback = function(callback) {
        this.errorHandler.setErrorCallback(callback);
        return this;
    }

    this.getFilter = function () {
        let _self = this;

        $.ajax({
            type: "POST",
            url: "/x/li/fg",
            data: {
                t: this.topic    //Topic der Liste
            },
            dataType: "json",
            success: function (response, status, xhr) {
                if (response.err)
                    _self.errorHandler.throwError('listView.getFilter-1', _self.errStr, response.err, xhr, status);

                else {
                    _self.listFilter = response.f;

                    for (let i = 0; i < _self.listFilter.length; i++) {
                        let f = _self.listFilter[i];
                        $('#f_' + this._colName2(f[i])).val()
                    }

                    _self.displayData();
                }
            },
            error: function (xhr, status, error) {
                _self.errorHandler.throwError('listView.getFilter-2', _self.errStr, '', xhr, status, error);
            }
        });
    };

    /** 
     * Senden des aktuellen Filterwertes für die Spalte per Ajax ans Backend
     * 
     * @param string colname
     * @param string value
     * @returns {Liste}
     */
    this.setFilter = function (colName, value) {
        let _self = this;

        this.listFilter[colName] = value;

        $.ajax({
            type: "POST",
            url: "/x/li/fs",
            data: {
                t: this.topic, //Topic der Liste
                c: colName,
                v: value
            },
            dataType: "json",
            success: function (response, status, xhr) {
                if (response.err)
                    _self.errorHandler.throwError('listView.setFilter-1', _self.errStr, response.err, xhr, status);

                else {
                    _self.listData = response.d;  // Lokales Zwischenspeichern der Daten
                    _self.displayData();
                }

            },
            error: function (xhr, status, error) {
                _self.errorHandler.throwError('listView.setFilter-2', _self.errStr, '', xhr, status, error);
            }
        });
        return this;
    };

    /**
     * Speichern des aktuellen Filterwertes und anzeigen der neu gefilterten Daten
     * 
     * @param string colName1
     * @param string colName2
     */
    this.filterData = function (colName) {
        this.setFilter(colName, $('#f_' + this._colName2(colName)).val());
        this.displayData();
    };

    /**
     * Abfragen der aktuellen Daten per Ajax-Request.
     */
    this.refreshData = function () {
        let _self = this;

        $.ajax({
            type: 'POST',
            url: '/x/li/d',
            data: {
                t: this.topic    //Topic der Liste
            },
            dataType: 'json',
            success: function (response, status, xhr) {
                if (response.err)
                    _self.errorHandler.throwError('listView.refreshData-1', _self.errStr, response.err, xhr, status);

                else {
                    _self.listData = response.d;  // Lokales Zwischenspeichern der Daten
                    _self.displayData();
                }
            },
            error: function (xhr, status, error) {
                _self.errorHandler.throwError('listView.refreshData-2', _self.errStr, '', xhr, status, error);
            }
        });
    };

    /**
     * Gefiltertes Anzeigen des übergebenen Datenarrays im Tabellenbereich der Liste.
     * 
     * Im ersten Element jeder Datenzeile steht der CSS-Klassenname für die 
     * Tabellenzeile gefolgt von den Werten für jede anzuzeigende Spalte.
     */
    this.displayData = function () {
        let newLocation = function (ziel) {
            return function () {
                window.location.replace(ziel);
            }
        }

        this.tData.innerHTML = '';

        this.refreshSelectValues();

        for (let i = 0; i < this.listData.length; i++) {
            let notInFilter = true;

            let row = this.listData[i];
            let rowData = row.rd;

            for (let key in this.listFilter) {
                let filterValue = this.listFilter[key];

                if (null !== filterValue) {
                    if (!rowData[key].toLowerCase().includes(filterValue.toLowerCase()))
                        notInFilter = false;
                }
            }

            if (notInFilter) {
                let tr = document.createElement('tr');

                if (null !== row.rl && row.rl.length > 0)
                    tr.onclick = newLocation(row.rl);

                if (null !== row.rc && row.rc.length > 0)
                    tr.className = row.rc;

                let cellClasses = row.cc;
                
                for (let key in this.listFilter) {
                    let td = document.createElement('td');
                    td.innerHTML = rowData[key];
                    
                    if (undefined !== cellClasses && null !== cellClasses && key in cellClasses && null !== cellClasses[key])
                        td.className = cellClasses[key];

                    tr.appendChild(td);
                }

                this.tData.appendChild(tr);
            }
        }
    };

    this.refreshSelectValues = function () {
        for (let key in this.listFilter) {
            if (this.searchModes[key] == this.SEARCH_SELECTION) {
                // Einträge im Drop-Down des Suchfilters der Spalte
                // aus der Wertemenge der Liste ermitteln
                let searchValues = [];

                for (let i = 0; i < this.listData.length; i++) {
                    let value = this.listData[i].rd[key];
                    if (searchValues.indexOf(value) < 0)
                        searchValues.push(value);
                }
                
                if (searchValues.indexOf('') < 0)
                    searchValues.push('');

                searchValues.sort();

                let selectBox = document.getElementById('f_' + this._colName2(key));
                selectBox.options.length = 0;   //Remove all options

                for (let i = 0; i < searchValues.length; i++) {
                    let opt = document.createElement("option");

                    if (searchValues[i] == this.listFilter[key])
                        opt.selected = 'selected';

                    opt.value = searchValues[i];
                    opt.textContent = searchValues[i];
                    selectBox.appendChild(opt);
                }
            }
        }
    };

    this.toggleSort = function (colName) {
        window.location.href = "?s=" + colName;     // Im Backend eine neu sortierte Liste anfordern
    };

    this._colName2 = function (colName) {
        return colName.replace('.', '_');
    };
    
    this.SEARCH_SELECTION = 's';
    this.SEARCH_TEXT = 't';
    this.errorHandler = new ErrorHandler();

    this.tData       = document.getElementById(tableId);
    this.topic       = this.tData.dataset.topic;
    this.errStr     = this.tData.dataset.err;
//    this.errStr2     = this.tData.dataset.err2;

    this.listData   = [];               // Lokale Kopie der (gefiltert) anzuzeigenden Daten
    this.listFilter = null;             // Aktuelle Filterbedinungen
    this.setSearchModes(this.tData.dataset.searchmodes)
        .setErrorCallback();            // Error-Callback initialisieren
    
    this.getFilter();
    this.refreshData();
}


ErrorHandler = function() {

    /**
     * Optionales Setzen eines Callbacks, der im Fehlerfalle aufgerufen wird.
     * Tag nochmal separat übergeben, damit ggf. bequemer geloggt werden kann.
     * 
     * @param {type} callback
     * @returns {Error}
     */
    this.setErrorCallback = function(callback) {
        this.errorCallback = (undefined === callback || null === callback) ? function(tag, hdg, msg) { alert(hdg + '\n' + msg); } : callback;
        return this;
    };

    /**
     * Prettyprint the error-details and forward to the error-callback for final
     * processing.
     * 
     * @param {type} tag
     * @param {type} hdg
     * @param {type} msg1
     * @param {type} xhr
     * @param {type} status
     * @param {type} error
     * @returns {undefined}
     */
    this.throwError = function(tag, hdg, msg1, xhr, status, error) {
        if (504 !== xhr.status && (0 !== xhr.status && 'error' !== status)) {
            let msg = tag + ': ';
            if (undefined !== msg1 && msg1.length > 0)   msg += 'Fehler: ' + msg1 + '\n';
            if (undefined !== status) msg += 'Status ' + status + ' ';
            if (undefined !== error && error.length > 0) msg += ' Errorcode ' + error + ' ';
            if (undefined !== xhr.status) msg += ' Status (xhr) ' + xhr.status + ' ';
            if (undefined !== xhr.responseText) msg += '\n' + xhr.responseText;

            this.errorCallback(tag, hdg, msg);      // Tag nochmal separat übergeben, damit ggf. bequemer geloggt werden kann
        }
    }
    
    this.setErrorCallback(null);
};

