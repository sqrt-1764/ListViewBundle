<?php

namespace Sqrt1764\ListViewBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sqrt1764\ListViewBundle\Classes\ListDefinition;
use Sqrt1764\ListViewBundle\Classes\ListRow;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * Ajax-Routinen für die Verwaltung der ListViews
 *
 * @author Matthias Leonhardt <matthias.leonhardt@gmail.com>
 */
class ListController extends AbstractController {
    public const HTML_SUCCESS               = 200;
    public const HTML_TEMPORARY_REDIRECT    = 307;
    public const HTML_ERR_BAD_REQUEST       = 400;
    public const HTML_INTERNAL_SERVER_ERROR = 500;

    /**
     * Index in the current PHP-session that contains the fully qualified name
     * of the class that instantiates the ListDefinition in ListController::queryListData
     *
     * @var string
     */
    public const LIST_ROWCLASS_SETTER = 'rcs';
    public const LIST_TOPIC           = 't';
    public const RESULT_ERROR         = 'err';

    private const DATA         = 'd';
    private const CELL_CLASSES = 'cc';
    private const ROW_CLASS    = 'rc';
    private const ROW_DATA     = 'rd';
    private const ROW_LINK     = 'rl';


    public function getUser1() {
        return $this->getUser();
    }


    /**
     * Anfage der Filtervalues der einzelnen Spalten der Liste
     *
     * @Route("/x/li/fg", name="sqrt1764_listview_getListFilter")
     *
     * @param Request $request
     * @return Response|JsonResponse
     */
    public function getListFilter(Request $request): Response {
        if (!$request->isXMLHttpRequest())
            return new Response('This is not ajax!', self::HTML_ERR_BAD_REQUEST);

        $topic = $request->get(self::LIST_TOPIC);

        $session = $request->getSession();
        $ld = new ListDefinition($session, $topic, $request);

        $filter = [];
        foreach ($ld->getColumns() as $col) {
            $filter[$col->getName()] = $col->getFilterValue();
        }
        $result['f'] = $filter;

        return new JsonResponse($result);
    }

    /**
     * Setzen des Filtervalues einer einzelnen Spalten auf einen neuen Wert
     *
     * @Route("/x/li/fs", name="sqrt1764_listview_setListFilter")
     *
     * @param Request $request
     * @return Response|JsonResponse
     */
    public function setListFilter(Request $request) {
        if (!$request->isXMLHttpRequest())
            return new Response('This is not ajax!', self::HTML_ERR_BAD_REQUEST);

        $topic = $request->get(self::LIST_TOPIC);
        $colName = $request->get('c');
        $filterValue = $request->get('v');

        $session = $request->getSession();
        $ld = new ListDefinition($session, $topic, $request);

        $ld->setFilterValue($request->getSession(), $colName, $filterValue);

            // Nun weiterleiten an die Routine zum Anzeigen der Daten unter
            // Verwendung des ursprünglichen Requests.
        return $this->redirectToRoute('sqrt1764_listview_listData', ['request' => $request], self::HTML_TEMPORARY_REDIRECT);
    }

/*
    public function getObjectManager(): \Doctrine\Common\Persistence\ObjectManager { return $this->getDoctrine()->getManager(); }

    public function getUser() {
        return $this->get('security.token_storage')->getToken()->getUser();
    }
*/

    /**
     *
     * @Route("/x/li/d", name="sqrt1764_listview_listData")
     *
     * @param Request $request
     */
    public function queryListData(Request $request): Response {
        if (!$request->isXMLHttpRequest())
            return new Response('This is not ajax!', self::HTML_ERR_BAD_REQUEST);

        $result = [];
        $returnCode = self::HTML_SUCCESS;
        try {
            $topic   = $request->get(self::LIST_TOPIC, null);
            $session = $request->getSession();

            $ldClass = $session->get(self::LIST_ROWCLASS_SETTER, []);
            $ldClass = (array_key_exists($topic, $ldClass)) ? $ldClass[$topic] : 'Sqrt1764\ListViewBundle\Classes\ListDefinition';

            $ld = new $ldClass($session, $topic, $request, $this);

            if (!($ld instanceof ListDefinition))
                throw new \InvalidArgumentException('ListDefinition::queryListData: Invalid class for the listDefinition');

            $result[self::DATA] = [];

            /* @var $data ListRow[] */
            $data = $ld->queryData($this->getDoctrine()->getManager(), false, $request);  // Filtering Data is performed by the client!

            foreach ($data as $rowData) {
                $row = [];
                $content = $rowData->getContent();

                foreach ($ld->getColumns() as $col) {
                    $colName = $col->getName();
                    $row[self::ROW_DATA][$colName] =  (array_key_exists($colName, $content) && (null !== $content[$colName])) ? $content[$colName] : '';
                }

                if (null !== $rowData->getCellClasses())
                    $row[self::CELL_CLASSES] = $rowData->getCellClasses();

                $row[self::ROW_CLASS] = $rowData->getRowClass();
                $row[self::ROW_LINK]  = $rowData->getItemDetailLink();

                $result[self::DATA][] = $row;
            }

        // @codeCoverageIgnoreStart
        } catch (\Exception $e) {
            $result[ListController::RESULT_ERROR] = $e->getMessage();
            $returnCode = self::HTML_INTERNAL_SERVER_ERROR;
//dump("Error:", $result);
//dump($e->getTraceAsString());
        }
        // @codeCoverageIgnoreEnd

        return new JsonResponse($result, $returnCode);
    }
}
