CONTAINER=leo-dev_fpm
WORKDIR="/var/www/html/Sqrt1764/ListViewBundle"

test:
	docker exec -w "${WORKDIR}" -it ${CONTAINER} sh -c "vendor/bin/simple-phpunit"

testfailing:
	vendor/bin/simple-phpunit --group failing
