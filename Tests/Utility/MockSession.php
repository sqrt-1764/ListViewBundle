<?php

/* 
 * License: See the file LICENSE in the rootfolder of this project
 */

namespace Sqrt1764\ListViewBundle\Tests\Utility;


/**
 * @author Matthias Leonhardt <matthias.leonhardt@gmail.com>
 */
class MockSession implements \Symfony\Component\HttpFoundation\Session\SessionInterface {
    
    public function all(): array { }
    public function clear() { }
    public function get($name, $default = null) { }
    public function getBag($name): \Symfony\Component\HttpFoundation\Session\SessionBagInterface { }
    public function getId(): string { }
    public function getMetadataBag(): \Symfony\Component\HttpFoundation\Session\Storage\MetadataBag { }
    public function getName() { }
    public function has($name): bool { }
    public function invalidate($lifetime = null): bool { }
    public function isStarted(): bool { }
    public function migrate($destroy = false, $lifetime = null): bool { }
    public function registerBag(\Symfony\Component\HttpFoundation\Session\SessionBagInterface $bag) { }
    public function remove($name) { }
    public function replace(array $attributes) { }
    public function save() { }
    public function set($name, $value) { }
    public function setId($id) { }
    public function setName($name) { }
    public function start(): bool { }
}