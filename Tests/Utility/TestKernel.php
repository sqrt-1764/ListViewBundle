<?php

namespace Sqrt1764\ListViewBundle\Tests\Utility;

use Symfony\Bundle\FrameworkBundle\Kernel\MicroKernelTrait;
use Symfony\Component\Config\Loader\LoaderInterface;
use Symfony\Component\Config\Resource\FileResource;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\HttpKernel\Kernel as BaseKernel;
use Symfony\Component\Routing\RouteCollectionBuilder;


/**
 * -> https://symfonycasts.com/screencast/symfony-bundle/controller-functional-test
 */
class TestKernel extends BaseKernel /* implements \Symfony\Component\DependencyInjection\Compiler\CompilerPassInterface */ {
    use MicroKernelTrait;

    const CONFIG_EXTS = '.{php,xml,yaml,yml}';
  
    public function getCacheDir() { return $this->getProjectDir().'/var/cache/'.$this->environment; }

    public function getLogDir()   { return $this->getProjectDir().'/var/log'; }
    
    public function registerBundles() {
        $contents = require $this->getProjectDir().'/Resources/config/bundles.php';
        foreach ($contents as $class => $envs) {
            if (isset($envs['all']) || isset($envs[$this->environment])) {
                yield new $class();
            }
        }
    }

    protected function configureContainer(ContainerBuilder $container, LoaderInterface $loader) {
        $confDir = $this->getProjectDir().'/Resources/config';
        
        $container->setParameter('container.dumper.inline_class_loader', true);
        $container->setParameter('kernel.secret', '%env(APP_SECRET)%');

        $loader->load($confDir.'/packages/*'.self::CONFIG_EXTS, 'glob');
        if (is_dir($confDir.'/packages/'.$this->environment)) {
            $loader->load($confDir.'/packages/'.$this->environment.'/**/*'.self::CONFIG_EXTS, 'glob');
        }
        $loader->load($confDir.'/services'.self::CONFIG_EXTS, 'glob');
        if (is_dir($confDir.'/services/'.$this->environment))
            $loader->load($confDir.'/services/'.$this->environment.self::CONFIG_EXTS, 'glob');
    }
    
    protected function configureRoutes(RouteCollectionBuilder $routes) {
        $confDir = $this->getProjectDir().'/Resources/config';
        if (is_dir($confDir.'/routes/'))
            $routes->import($confDir.'/routes/*'.self::CONFIG_EXTS, '/', 'glob');

        if (is_dir($confDir.'/routes/'.$this->environment))
            $routes->import($confDir.'/routes/'.$this->environment.'/**/*'.self::CONFIG_EXTS, '/', 'glob');
        $routes->import($confDir.'/routes'.self::CONFIG_EXTS, '/', 'glob');
    }
 }
