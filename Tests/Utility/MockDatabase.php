<?php

namespace Sqrt1764\ListViewBundle\Tests\Utility;

/**
 * Erzeuge eine In-Memory SQLIte-Datenbank für Unit-Tests
 *
 * @author Matthias Leonhardt <matthias.leonhardt@gmail.com>
 */
class MockDatabase /* implements IDatabase */ {
    public static function create(string $drivername, array $tables): MockDatabase {
        $db = new MockDatabase(new \PDO($drivername));
        
        foreach ($tables as $tablename => $schema) {
            $db->query('DROP TABLE IF EXISTS '.$tablename);
            $db->query('CREATE TABLE `'.$tablename.'` ('.$schema.')');
        }
        
        return $db;
    }

    private $pdo;
    
    public function __construct($mockedPDO) {
        $this->pdo = $mockedPDO;
    }
    
    public function query(string $querystring): \PDOStatement { 
        try {
            $statement = $this->pdo->query($querystring);
            if (false === $statement)
                throw new \Exception(self::LOG_TAG."::query: Invalid Query: ".$querystring);

        } catch (\Exception $e) {
            throw new \Exception(self::LOG_TAG."::query: Invalid Query: ".$querystring."<br />"."Error: ".$e->getMessage());
        }

        return $statement;
    }
}
