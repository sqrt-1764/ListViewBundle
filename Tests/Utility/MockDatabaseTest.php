<?php

namespace Sqrt1764\ListViewBundle\Tests\Utility;

use PHPUnit\Framework\TestCase;

/**
 * Test that the MockDatabase is functional
 * 
 * @author Matthias Leonhardt <matthias.leonhardt@gmail.com>
 */
class MockDatabaseTest extends TestCase {
    
    private $_db;
    
    protected function tearDown() {
        parent::tearDown();
        $this->_db = null;
    }
    
    private function create() {
        $tables = [
                    'testtable1' => '`id` INT(11) NOT NULL,'
                                    .'`data1` VARCHAR(40),' 
                                    .'`created_at` INT(11)'
                    ];
        
        $this->_db = MockDatabase::create('sqlite::memory:', $tables);
    }
    
    protected function getDb(): MockDatabase {
        if (null === $this->_db)
            $this->create();
        
        return $this->_db;
    }


    public function testInsert() {
        $db = $this->getDb();
        
        $rows = $db->query('SELECT count(*) FROM testtable1')->fetchColumn();
        $this->assertEquals(0, $rows);
        
        $db->query('INSERT INTO testtable1 VALUES (1, "Test 1", 12345)');
        $rows = $db->query('SELECT count(*) FROM testtable1')->fetchColumn();
        $this->assertEquals(1, $rows);
        
        $db->query('INSERT INTO testtable1 VALUES (2, "Test 2", 12345)');
        $rows = $db->query('SELECT count(*) FROM testtable1')->fetchColumn();
        $this->assertEquals(2, $rows);
    }
}
