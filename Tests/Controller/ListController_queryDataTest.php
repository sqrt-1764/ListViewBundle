<?php

/* 
 * License: See the file LICENSE in the rootfolder of this project
 */

namespace Sqrt1764\ListViewBundle\Tests\Controller;

use Sqrt1764\ListViewBundle\Classes\ListDefinition;
use Sqrt1764\ListViewBundle\Controller\ListController;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Session\SessionInterface;

/**
 * 
 * @author Matthias Leonhardt <matthias.leonhardt@gmail.com>
*/
class ListController_queryDataTest extends WebTestCase {
    
    private const TESTROUTE = '/x/li/d';
    
    private $client;
    
    public function setUp() {
        parent::setUp();
        
        $this->client = static::createClient();
    }

    /* Check that an empty request produces an error-response */
    public function testEmtpyRequest() {
        $this->client->request('GET', self::TESTROUTE);
        
        $response = $this->client->getResponse();
        $this->assertSame(ListController::HTML_ERR_BAD_REQUEST, $response->getStatusCode(), 'Es wird Fehlercode 400 zurückgemeldet');
    }
    
    /* Check that a request with invalid query-parameters produces an error-response */
    public function testBadRequestParameters() {
        $this->client->request('GET', self::TESTROUTE.'?xy=hallo');
        
        $response = $this->client->getResponse();
        $this->assertSame(ListController::HTML_ERR_BAD_REQUEST, $response->getStatusCode(), 'Es wird Fehlercode 400 zurückgemeldet');
    }

    public function testDefaultRowClassSetter() {
        $this->client->xmlHttpRequest('GET', self::TESTROUTE, [
            ListController::LIST_TOPIC           => 'test1',
        ]);
        
        $response = $this->client->getResponse();
        $this->assertSame(ListController::HTML_SUCCESS, $response->getStatusCode(), 'Server reports an error');
    }
    
/* TODO Die Klasse für die ListDefinition wird im Moment in der Session gespeichert
    public function testInvalidRowClassSetter() {
        $this->client->xmlHttpRequest('GET', self::TESTROUTE, [
            ListController::LIST_TOPIC           => 'test1',
            ListController::LIST_ROWCLASS_SETTER => 'Sqrt1764\ListViewBundle\Tests\Controller\MockListDefinitionInvalid'
        ]);
        
        $response = $this->client->getResponse();
        $this->assertSame(ListController::HTML_INTERNAL_SERVER_ERROR, $response->getStatusCode(), 'Server reports an error');
    }
/**/
    
    public function test1() {
        $this->markTestIncomplete('Muß erst noch Testdaten generieren!');
        
        $this->client->xmlHttpRequest('GET', self::TESTROUTE, [
            ListController::LIST_TOPIC           => 'test1',
            ListController::LIST_ROWCLASS_SETTER => 'Sqrt1764\ListViewBundle\Tests\Controller\MockListDefinition'
        ]);
        
        $response = $this->client->getResponse();
        $this->assertSame(ListController::HTML_SUCCESS, $response->getStatusCode(), 'Es wird Erfolgscode 200 zurückgemeldet');
    }
}

class MockListDefinition extends ListDefinition {
    
    public function __construct(SessionInterface $session, string $session_key, Request $request = null, ListController $callingController = null) {
        parent::__construct($session, $session_key, $request, $callingController);
        
        $this->rowClassSetter = function($item, $colname = null) /*use($om, $user)*/ {
            if (null === $colname && $item->getId() === 'id1') return 'rowClass1';
            if ('col1' === $colname && $item->getId() === 'id1') return 'cellClass11';
            if ('col2' === $colname) return 'cellClass2';
        };
    }
}
    
class MockListDefinitionInvalid {
    
    public function __construct(SessionInterface $session, string $session_key, Request $request = null, ListController $callingController = null) { }
}
