<?php

/* 
 * License: See the file LICENSE in the rootfolder of this project
 */

namespace Sqrt1764\ListViewBundle\Tests\Classes;

use Sqrt1764\ListViewBundle\Classes\ListColumnDef;
use Symfony\Bundle\FrameworkBundle\Tests\TestCase;

/**
 * @author Matthias Leonhardt
 */
class ListColumnDef_getItemValueTest extends TestCase {
    
    /**
     * Get the value for the column directly from the column of the data-row
     * in case the data-row is an array.
     */
    function testArray1() {
        $lcd = new ListColumnDef('col1', 'ColHeading1');
        
        $value = $lcd->getItemValue(['col0' => 'Value 0', 'col1' => 'Value 1', 'col2' => 'Value 2']);
        $this->assertSame('Value 1', $value, 'Spaltenwert aus Array-Zeile extrahiert');
    }
    
    /**
     * Get the value for the column directly from the column of the data-row
     * in case the data-row is an array and the column-name is prefixed with 
     * the name of a table.
     */
    function testArray2() {
        $lcd = new ListColumnDef('table.col1', 'ColHeading1');
        
        $value = $lcd->getItemValue(['col0' => 'Value 0', 'col1' => 'Value 1', 'col2' => 'Value 2']);
        $this->assertSame('Value 1', $value, 'Spaltenwert aus Array-Zeile extrahiert');
    }
    
    /**
     * Get the value for the column directly from the column of the data-row
     * in case the data-row is an object.
     */
    function testObject1() {
        $lcd = new ListColumnDef('col1', 'ColHeading1');
        
        $item = new \stdClass();
        $item->col0 = 'Value 0';
        $item->col1 = 'Value 1';
        $item->col2 = 'Value 2';

        $value = $lcd->getItemValue($item);
        $this->assertSame('Value 1', $value, 'Spaltenwert aus object-member extrahiert');
    }
    
    /**
     * Get the value for the column directly from the column of the data-row
     * in case the data-row is an object and the column-name is prefixed with 
     * the name of a table.
     */
    function testObject2() {
        $lcd = new ListColumnDef('table.col1', 'ColHeading1');
        
        $item = new \stdClass();
        $item->col0 = 'Value 0';
        $item->col1 = 'Value 1';
        $item->col2 = 'Value 2';

        $value = $lcd->getItemValue($item);
        $this->assertSame('Value 1', $value, 'Spaltenwert aus object-member extrahiert');
    }

    function testGetterArray1() {
        $lcd = new ListColumnDef('col1', 'ColHeading1', 'getCol1');
        
        $value = $lcd->getItemValue(['col0' => 'Value 0', 
                                     'col1' => new _TestData("id1", "Value 00", 'Value 01', 'Value 02'),
                                     'col2' => 'Value 2']);
        $this->assertSame('##TestData->Value 01', $value, 'Getter auf dem Datenobjekt in Spalte col1 ausgeführt');
    }
    
    function testGetterArray2() {
        $lcd = new ListColumnDef('table.col1', 'ColHeading1', 'getCol1');
        
        $value = $lcd->getItemValue(['col0' => 'Value 0', 
                                     'col1' => new _TestData("id1", "Value 00", 'Value 01', 'Value 02'),
                                     'col2' => 'Value 2']);
        $this->assertSame('##TestData->Value 01', $value, 'Getter auf dem Datenobjekt in Spalte col1 ausgeführt');
    }
    
    /**
     * Test that a member-function of the data-object can be used to return the value
     * for this column from the data-object.
     */
    function testGetterObject1() {
        $lcd = new ListColumnDef('col1', 'ColHeading1', 'getCol1');
        
        $value = $lcd->getItemValue(new _TestData('id1', 'Value 0', 'Value 1', 'Value 2'));
        $this->assertSame('##TestData->Value 1', $value, 'Spaltenwert aus Array-Zeile extrahiert');
    }
    
    function testGetterObject2() {
        $lcd = new ListColumnDef('table.col1', 'ColHeading1', 'getCol1');
        
        $value = $lcd->getItemValue(new _TestData('id1', 'Value 0', 'Value 1', 'Value 2'));
        $this->assertSame('##TestData->Value 1', $value, 'Spaltenwert aus Array-Zeile extrahiert');
    }
}
