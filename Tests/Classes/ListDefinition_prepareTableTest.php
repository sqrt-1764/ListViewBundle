<?php

/* 
 * License: See the file LICENSE in the rootfolder of this project
 */

namespace Sqrt1764\ListViewBundle\Tests\Classes;

use Sqrt1764\ListViewBundle\Classes\ListColumnDef;
use Sqrt1764\ListViewBundle\Classes\ListDefinition;
use Sqrt1764\ListViewBundle\Classes\ListRow;
use Sqrt1764\ListViewBundle\Tests\Utility\MockSession;
use Symfony\Bundle\FrameworkBundle\Tests\TestCase;

/**
 * @author Matthias Leonhardt <matthias.leonhardt@gmail.com>
 */
class ListDefinition_prepareTableTest extends TestCase {
    private const SESSION_KEY = "xy";
    private const LISTROOT    = "/list";
    
    private $ld;
    private $dataItems;
    
    public function setUp() {
        parent::setUp();
        
        $session = new MockSession();
        $this->ld = new ListDefinition($session, self::SESSION_KEY);
        
        $this->ld->setEditRoute(self::LISTROOT)
                ->addColumn(new ListColumnDef('col0', 'Column 0', 'getCol0'))
                ->addColumn(new ListColumnDef('col1', 'Column 1'))
                ->addColumn(new ListColumnDef('col2', 'Column 2'));
        
        $this->dataItems = [];
        $this->dataItems[] = new _TestData('id0', '00', '01', '02');
        $this->dataItems[] = new _TestData('id1', '10', '11', '12');
        $this->dataItems[] = new _TestData('id2', '20', '21', '22');
    }
    
    public function testItemDetailLink() {
        $tableData = $this->ld->prepareTable($this->dataItems);
        
        $this->assertEquals(3, count($tableData), 'Korrekte Anzahl von Tabellenzeilen gerendert');

        /* @var $row ListRow  */
        $row = $tableData[0];
        $this->assertEquals(self::LISTROOT.'/id0',                                          $row->getItemDetailLink(), '');
        $this->assertEquals(" onclick='window.location.href = \"".self::LISTROOT."/id0\"'", $row->getItemDetailLink1(), '');

        $row = $tableData[1];
        $this->assertEquals(self::LISTROOT.'/id1',                                          $row->getItemDetailLink(), '');
        $this->assertEquals(" onclick='window.location.href = \"".self::LISTROOT."/id1\"'", $row->getItemDetailLink1(), '');

        $row = $tableData[2];
        $this->assertEquals(self::LISTROOT.'/id2',                                          $row->getItemDetailLink(), '');
        $this->assertEquals(" onclick='window.location.href = \"".self::LISTROOT."/id2\"'", $row->getItemDetailLink1(), '');
    }
    
    public function testGetContent() {
        $tableData = $this->ld->prepareTable($this->dataItems);
        
        $this->assertEquals(3, count($tableData), 'Korrekte Anzahl von Tabellenzeilen gerendert');

        /* @var $row ListRow  */
        $row = $tableData[0];
        $this->assertEquals($this->dataItems[0]->getCol0(), $row->getContent1('col0'), 'Daten des ValueGetters werden angezeigt');
        $this->assertEquals($this->dataItems[0]->col1,      $row->getContent1('col1'), 'Rohdaten werden angezeigt');
        $this->assertEquals($this->dataItems[0]->col2,      $row->getContent1('col2'), 'Rohdaten werden angezeigt');

        $row = $tableData[1];
        $this->assertEquals($this->dataItems[1]->getCol0(), $row->getContent1('col0'), 'Daten des ValueGetters werden angezeigt');
        $this->assertEquals($this->dataItems[1]->col1,      $row->getContent1('col1'), 'Rohdaten werden angezeigt');
        $this->assertEquals($this->dataItems[1]->col2,      $row->getContent1('col2'), 'Rohdaten werden angezeigt');

        $row = $tableData[2];
        $this->assertEquals($this->dataItems[2]->getCol0(), $row->getContent1('col0'), 'Daten des ValueGetters werden angezeigt');
        $this->assertEquals($this->dataItems[2]->col1,      $row->getContent1('col1'), 'Rohdaten werden angezeigt');
        $this->assertEquals($this->dataItems[2]->col2,      $row->getContent1('col2'), 'Rohdaten werden angezeigt');
    }
    
    public function testPlain() {
        $tableData = $this->ld->prepareTable($this->dataItems);
        
        $this->assertEquals(3, count($tableData), 'Korrekte Anzahl von Tabellenzeilen gerendert');

        /* @var $row ListRow  */
        $row = $tableData[0];
        $this->assertNull($row->getRowClass(),                 'No rowClass set');
        $this->assertEquals('', $row->getRowClass1(),          'No rowClass set');

        $this->assertNull($row->getCellClass('col0'),          'No cellClass set for col0');
        $this->assertEquals('', $row->getCellClass1('col0'),   'No cellClass set for col0');
        $this->assertNull($row->getCellClass('col1'),          'No cellClass set for col1');
        $this->assertEquals('', $row->getCellClass1('col1'),   'No cellClass set for col1');
        $this->assertNull($row->getCellClass('col2'),          'No cellClass set for col2');
        $this->assertEquals('', $row->getCellClass1('col2'),   'No cellClass set for col2');

        
        $row = $tableData[1];
        $this->assertNull($row->getRowClass(),                 'No rowClass set');
        $this->assertEquals('', $row->getRowClass1(),          'No rowClass set');

        $this->assertNull($row->getCellClass('col0'),          'No cellClass set for col0');
        $this->assertEquals('', $row->getCellClass1('col0'),   'No cellClass set for col0');
        $this->assertNull($row->getCellClass('col1'),          'No cellClass set for col1');
        $this->assertEquals('', $row->getCellClass1('col1'),   'No cellClass set for col1');
        $this->assertNull($row->getCellClass('col2'),          'No cellClass set for col2');
        $this->assertEquals('', $row->getCellClass1('col2'),   'No cellClass set for col2');

        
        $row = $tableData[2];
        $this->assertNull($row->getRowClass(),                 'No rowClass set');
        $this->assertEquals('', $row->getRowClass1(),          'No rowClass set');

        $this->assertNull($row->getCellClass('col0'),          'No cellClass set for col0');
        $this->assertEquals('', $row->getCellClass1('col0'),   'No cellClass set for col0');
        $this->assertNull($row->getCellClass('col1'),          'No cellClass set for col1');
        $this->assertEquals('', $row->getCellClass1('col1'),   'No cellClass set for col1');
        $this->assertNull($row->getCellClass('col2'),          'No cellClass set for col2');
        $this->assertEquals('', $row->getCellClass1('col2'),   'No cellClass set for col2');
    }
    
    public function testRowClassSetter() {
        $this->ld->setRowClassSetter(function ($item, string $colname = null) {
            if (null === $colname && $item->getId() === 'id1') return 'rowClass1';
            if ('col1' === $colname && $item->getId() === 'id1') return 'cellClass11';
            if ('col2' === $colname) return 'cellClass2';
            
            return null;
        });
        
        $tableData = $this->ld->prepareTable($this->dataItems);
        
        $this->assertEquals(3, count($tableData), 'Korrekte Anzahl von Tabellenzeilen gerendert');

        /* @var $row ListRow  */
        $row = $tableData[0];
        $this->assertNull($row->getRowClass(),                 'No rowClass set');
        $this->assertEquals('', $row->getRowClass1(),          'No rowClass set');

        $this->assertNull($row->getCellClass('col0'),                          'No cellClass set for col0');
        $this->assertEquals('',                   $row->getCellClass1('col0'), 'No cellClass set for col0');
        $this->assertNull($row->getCellClass('col1'),                          'No cellClass set for col1');
        $this->assertEquals('',                   $row->getCellClass1('col1'), 'No cellClass set for col1');
        $this->assertEquals('cellClass2',         $row->getCellClass('col2'),  'cellClass2 set for col2');
        $this->assertEquals('class="cellClass2"', $row->getCellClass1('col2'), 'cellClass2 set for col2');

        
        $row = $tableData[1];
        $this->assertEquals('rowClass1',          $row->getRowClass(),  'rowClass is set for row 1');
        $this->assertEquals(' class="rowClass1"', $row->getRowClass1(), 'rowClass is set for row 1');

        $this->assertNull($row->getCellClass('col0'),                           'No cellClass set for col0');
        $this->assertEquals('',                    $row->getCellClass1('col0'), 'No cellClass set for col0');
        $this->assertEquals('cellClass11',         $row->getCellClass('col1'),  'cellClass11 set for col1');
        $this->assertEquals('class="cellClass11"', $row->getCellClass1('col1'), 'cellClass11 set for col1');
        $this->assertEquals('cellClass2',          $row->getCellClass('col2'),  'cellClass2 set for col2');
        $this->assertEquals('class="cellClass2"',  $row->getCellClass1('col2'), 'cellClass2 set for col2');

        
        $row = $tableData[2];
        $this->assertNull($row->getRowClass(),                 'No rowClass set');
        $this->assertEquals('', $row->getRowClass1(),          'No rowClass set');

        $this->assertNull($row->getCellClass('col0'),                          'No cellClass set for col0');
        $this->assertEquals('',                   $row->getCellClass1('col0'), 'No cellClass set for col0');
        $this->assertNull($row->getCellClass('col1'),                          'No cellClass set for col1');
        $this->assertEquals('',                   $row->getCellClass1('col1'), 'No cellClass set for col1');
        $this->assertEquals('cellClass2',         $row->getCellClass('col2'),  'cellClass2 set for col2');
        $this->assertEquals('class="cellClass2"', $row->getCellClass1('col2'), 'cellClass2 set for col2');
    }
}
