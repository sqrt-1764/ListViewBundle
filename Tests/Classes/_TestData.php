<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Sqrt1764\ListViewBundle\Tests\Classes;

/** Testclass for testGetItemValue_GetterObject() */
class _TestData {
    public $id;
    public $col0;
    public $col1;
    public $col2;

    public function __construct(string $id, string $col0, string $col1, string $col2) {
        $this->id   = $id;
        $this->col0 = $col0;
        $this->col1 = $col1;
        $this->col2 = $col2;
    }
        
    public function getId():   string { return $this->id; }
    public function getCol0(): string { return "##TestData->".$this->col0; }
    public function getCol1(): string { return "##TestData->".$this->col1; }
    public function getCol2(): string { return "##TestData->".$this->col2; }
}
    
