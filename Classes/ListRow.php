<?php

use Sqrt1764\ListViewBundle\Classes\ListDefinition;

namespace Sqrt1764\ListViewBundle\Classes;

/**
 * Eine anzuzeigende Zeile in der Tabellenansicht
 * Enthält neben den anzuzeigenden Spaltenwerten auch einen Verweis auf das
 * zugrundeliegende Objekt um Detail
 *
 * @author Matthias Leonhardt <matthias.leonhardt@gmail.com>
 */
class ListRow {
    
    /**
     * ListDefinition, die diese Zeile erzeugt hat
     * 
     * @var ListDefinition
     */
    private $listDefinition;
    
    /**
     * @var string
     */
    private $detail_url;

    /**
     * @var string
     */
    private $rowCssClass;

    /**
     * @var array
     */
    private $cellCssClasses;
    
    /**
     * Anzuzeigende Werte für die jeweiligen Spalten
     * 
     * @var string[]
     */
    private $content;
    
    /**
     * @param ListDefinition $ld
     * @param string|null    $detail_url
     * @param array          $content       Assoziatives Array mit den Werten für die jeweiligen Spalten
     * @param string         $rowCssClass   Name of the css class(es) that apply for this row
     * @param array          $cellCssClass  Name of the css class(es) that apply for 
     *                                      the collesponding value of this row
     */
    public function __construct(ListDefinition $ld, ?string $detail_url, array $content, string $rowCssClass = null, array $cellCssClasses = null) {
        $this->listDefinition = $ld;
        $this->detail_url     = $detail_url;
        $this->content        = $content;
        $this->rowCssClass    = $rowCssClass;
        $this->cellCssClasses = $cellCssClasses;
    }
    
    public function getItemDetailLink(): ?string  {  return $this->detail_url;  }
    public function getItemDetailLink1(): string { 
        return (null === $this->detail_url) ? '' : " onclick='window.location.href = \"".$this->detail_url."\"'"; 
    }

    public function getRowClass(): ?string       { return $this->rowCssClass; }
    public function getRowClass1(): string       { return null === $this->rowCssClass ? '' : ' class="'.$this->rowCssClass.'"'; }
    
    public function getCellClass(string $colName): ?string { 
        if (null === $this->cellCssClasses) return null;
        return (!array_key_exists($colName, $this->cellCssClasses) || null === $this->cellCssClasses[$colName] || strlen($this->cellCssClasses[$colName]) == 0) 
                    ? null
                    : $this->cellCssClasses[$colName]; 
    }
    public function getCellClass1(string $colName): string { 
        if (null === $this->cellCssClasses) return '';
        return (!array_key_exists($colName, $this->cellCssClasses) || null === $this->cellCssClasses[$colName] || strlen($this->cellCssClasses[$colName]) == 0) 
                    ? '' 
                    : 'class="'.$this->cellCssClasses[$colName].'"'; 
    }
    public function getCellClasses(): ?array     { return $this->cellCssClasses; } 
    
    /**
     * @return array    Anzuzeigende Werte für die korrespondierende Spalte
     *                  Darf HTML-Code enthalten! (Wird in Twig mit | raw gefiltert
     */
    public function getContent(): array { 
        return $this->content; 
    }
    public function getContent1(string $colName) { 
        return $this->content[$colName]; 
    }

}
