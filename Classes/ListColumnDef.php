<?php

namespace Sqrt1764\ListViewBundle\Classes;

//use JsonSerializable;

/**
 * Description of ListColumnDef
 *
 * @author Matthias Leonhardt
 */
class ListColumnDef /* implements JsonSerializable */ {
    public const SORT_ASC  = 'ASC';
    public const SORT_DESC = 'DESC';
    public const SORT_NONE = 'NONE';
    public const SORT_NOT_SORTABLE = 'NOT_SORTABLE';
    
    public const SEARCH_SELECTION = 's';
    public const SEARCH_TEXT      = 't';

    /**
     * @var string
     */
    private $heading;
    
    /**
     * @var string
     */
    private $name;
    
    /**
     * @var string
     */
    private $nameExported;
    
    /**
     * @var bool
     */
    private $searchable;
    

    /**
     * String nach dem diese Spalte gefiltert werden soll
     * 
     * @var string
     */
    private $filtervalue = null;

    /**
     * @var string
     */
    private $sortMode = self::SORT_NONE;
    
    /**
     * @var string
     */
    private $searchMode = self::SEARCH_TEXT;
    
    /**
     * Name of a member-function of the data-object that is called to get the
     * value of the data-object for this column.
     * Works only if the data-object is an object, not if the data-object is an
     * array!
     * 
     * @var string
     */
    private $valueGetter;
    
    
    /**
     * @param string $colName       Name of the column in the data-table that corresponds to
     *                              this column of the listview.
     * @param string $heading       Heading of the column in the listview
     * @param string $valueGetter   Name der Methode, die den anzuzeigenden Wert dieser Spalte des Objekts zurück liefert
     * @param string $sortmode
     * @param bool   $searchable    Flag, ob die Liste nach dieser Spalte gefiltert werden kann - entsprechend
     *                              wird dann passendes Eingabefeld über dem Tabellenkopf erzeugt.
     * @param string $filtervalue
     */
    public function __construct(string $colName, string $heading, $valueGetter = null, string $sortmode = self::SORT_NONE, bool $searchable = true, string $filtervalue = null) {
        $this->name         = $colName;
        $this->valueGetter  = $valueGetter;
        $this->heading      = $heading;
        $this->searchable   = $searchable;
        $this->sortMode     = $sortmode;
        $this->filtervalue  = $filtervalue;
    }
    
    public function isSearchable(): bool              { return $this->searchable; }

    public function getHeading(): string              { return $this->heading; }

    public function getName(): string                 { return (null === $this->nameExported) ? $this->name : $this->nameExported; }
    public function getName1(): string                { return str_replace('.', '_', $this->getName()); }
    
    public function setNameExported(?string $name): self {
        $this->nameExported = $name;
        return $this;
    }
    
    public function getFiltervalue(): ?string         { return $this->filtervalue; }
    public function setFiltervalue(?string $s): self  { $this->filtervalue = $s; return $this; }
    
    public function getSearchMode(): string           { return $this->searchMode; }
    public function setSearchMode(string $mode): self { $this->searchMode = $mode; return $this; }
    
    public function getValueGetter()                  { return $this->valueGetter; }
    public function setValueGetter($getter): self     { $this->valueGetter = $getter; return $this; }
    
    /**
     * Get the value from the item for this column of the listview.
     * 
     * @param array|stdClass    $rowData
     * @return type
     */
    public function getItemValue($rowData) {
        $colName = $this->name;
        $i = strpos($colName, '.');
        if ($i > 0) 
            $colName = substr($colName, $i+1);
        
        if (null === $this->valueGetter)
            return ('array' === gettype($rowData)) ? $rowData[$colName] : $rowData->$colName;
        else
            return ('array' === gettype($rowData)) 
                    ? call_user_func([$rowData[$colName], $this->valueGetter])
                    : call_user_func([$rowData, $this->valueGetter]);
    }
    
    
    public function getSortIndicator(): string  { 
        switch ($this->sortMode) {
            case self::SORT_ASC:  return '&#xF0DE;'; // '<div class="fa fa-sort-asc" />';
            case self::SORT_DESC: return '&#xF0DD;'; // '<div class="fa fa-sort-desc" />';
            case self::SORT_NONE: return '&#xF0DC;'; // <div class="fa fa-sort" />';
            default: return "";
        }
    }
    
    public function getSortMode(): string             { return $this->sortMode; }
    public function setSortMode(string $s): self { 
        if (!in_array($s, [self::SORT_ASC, self::SORT_DESC, self::SORT_NONE, self::SORT_NOT_SORTABLE]))
                throw new \Exception("Invalid sort-mode ".$s." selected!");
        
        $this->sortMode = $s; 
        return $this; 
    }
    
    public function toggleSortMode(): string {
        switch ($this->sortMode) {
            case self::SORT_ASC:  $this->sortMode = self::SORT_DESC; break;
            case self::SORT_DESC: $this->sortMode = self::SORT_NONE; break;
            case self::SORT_NONE: $this->sortMode = self::SORT_ASC; break;
            default:              $this->sortMode = self::SORT_NOT_SORTABLE; break;
        }

        return $this->sortMode;
    }
}
