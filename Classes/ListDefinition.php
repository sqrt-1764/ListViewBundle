<?php

namespace Sqrt1764\ListViewBundle\Classes;

use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\SessionInterface;

/**
 * Beschreibt die Struktur einer anzuzeigenden Liste.
 *
 * Diese Liste kann nach den einzelnen Spalten sortiert und gefiltert werden.
 * Entsprechendes regeln dann die Spaltenobjekte.
 *
 * @author Matthias Leonhardt <matthias.leonhardt@gmail.com>
 */
class ListDefinition {

    /**
     * Key in die aktive PHP-Session wo der aktuelle Zustand der Liste persistiert wird.
     * @var string
     */
    protected $sessionKey;

    protected $actionmenuKey;

    protected $editroute;

    protected $rowClassSetter;

    protected $dql;

    protected $heading;

    protected $title;

    protected $columns = [];

    protected $callingController;


    /**
     *
     * @param SessionInterface  $session
     * @param string            $session_key
     * @param Request           $request      Enthält die aktuellen Zustände der Spalten (Filter und Sortierung)
     */
    public function __construct(SessionInterface $session, string $session_key, Request $request = null, AbstractController $controller = null) {
        $this->sessionKey = $session_key;
        $this->callingController = $controller;

        //Lesen des vorigen Standes aus der Session
        if (null !== $session->get($session_key)) {
            $x = unserialize($session->get($session_key));
            $this->actionmenuKey = $x->actionmenuKey;
            $this->dql       = $x->dql;
            $this->editroute = $x->editroute;
            $this->heading   = $x->heading;
            $this->columns   = $x->columns;
        }

        //Aktualisieren des Status mit den Daten aus dem Request
        if (null !== $request  && null !== $request->get('f')) {
            $this->setFilterValue($session, $request->get('f'), $request->get('v'));
            $changed = true;
        }

        if (null !== $request && null !== $request->get('s'))
            $this->toggleSortMode($session, $request->get('s'));

        $this->writeStatus($session);

        return $this;
    }

    public function __sleep() {
        return ['sessionKey', 'actionmenuKey', 'editroute', 'dql', 'heading' , 'title', 'columns'];
    }


    public function setActionMenuKey(string $key): self { $this->actionmenuKey = $key; return $this; }

    public function setDql(string $dql): self           { $this->dql = $dql; return $this; }

    public function setEditRoute(string $route): self {
        if (strpos($route, '/') != strlen($route)) $route .= '/';
        $this->editroute = $route;
        return $this;
    }

    public function setHeading(string $s): self         { $this->heading = $s; return $this; }

    public function setTitle(string $s): self           { $this->title = $s; return $this; }

    public function setRowClassSetter($x): self         { $this->rowClassSetter = $x; return $this; }

    public function addColumn(ListColumnDef $col): self { $this->columns[$col->getName()] = $col; return $this; }
    public function getColumns(): array                 { return $this->columns; }
    public function clearColumns(): self                { $this->columns = []; return $this; }

    public function setFilterValue(SessionInterface $session, string $colname, ?string $value): self {
        $this->columns[$colname]->setFilterValue($value);
        $this->writeStatus($session);
        return $this;
    }

    public function getSearchModesJSON(): string {
        $modes = [];
        foreach ($this->columns as $col)
            $modes[$col->getName()] = $col->getSearchMode();

        return json_encode($modes);
    }

    public function toggleSortMode(SessionInterface $session, string $colname) {
        $this->columns[$colname]->toggleSortMode();
        $this->writeStatus($session);
    }

    /**
     * Speichere den aktuellen Listenstatus in der aktiven PHP-Session
     */
    public function writeStatus(SessionInterface $session) {
        $session->set($this->sessionKey, serialize($this));
    }

    /**
     * Generate the html-page to display the data
     *
     *
     *
     * @param \Twig_Environment $twig
     * @param ObjectManager     $om
     * @param array             $errors
     * @param array             $parameters1
     * @return Response
     */
    public function listItems(\Twig_Environment $twig, ObjectManager $om, array $errors = null, array $parameters1 = null): Response {
        $parameters = [
                'listDefinition' => $this,
//                'data'           => $this->queryData($om),
                'topic'          => $this->sessionKey,
                'admin_user'     => true
              ];

        if (null !== $errors)
            $parameters['errors'] = $errors;

        if (null !== $this->heading && strlen($this->heading) > 0)
            $parameters['heading'] = $this->heading;

        if (null !== $this->title && strlen($this->title) > 0)
            $parameters['title'] = $this->title;

        if (null !== $this->actionmenuKey && strlen($this->actionmenuKey) > 0)
            $parameters['actionMenuId'] = $this->actionmenuKey;

        if (null !== $parameters1)
            $parameters = array_merge($parameters, $parameters1);

        $response = new Response();
        $response->setContent($twig->render('@Sqrt1764ListView\items.list.html.twig', $parameters));

        return $response;
    }

   /**
     * Datenbankabfrage für die anzuzeigenden Objekte.
     *
     * Hier wird dann nach den aktuellen Bedingungen gefiltert und die Ergebnismenge
     * entsprechend den Sortiermodi der Spalten sortiert.
     *
     * @param ObjectManager $om
     * @param bool          $filterData
     * @return              array
     */
    public function queryData(ObjectManager $om, bool $filterData = true, Request $request = null): array {
        if (null === $this->dql || strlen($this->dql) == 0) return [];

            // Objektabfrage per DQL (QueryBuilder)
        $order = '';
        foreach ($this->columns as $col)
            if (in_array($col->getSortMode(), [ListColumnDef::SORT_ASC, ListColumnDef::SORT_DESC])) {
                if (strlen($order) > 0) $order .= ',';
                $order .= $col->getName()." ".$col->getSortMode();
            }

        $dql = "";

        if ($filterData) {
            $customFilter = false;

            foreach ($this->columns as $col) {
                $s = $col->getFiltervalue();
                if ($s !== null && strlen($s) > 0) {
                    if ($s != null && strlen($s) > 0) {
                        if (strlen($dql) > 0) $dql .= " AND ";
                        $dql .= $col->getName()." LIKE '%".$s."%'";
                    }
                }
            }
        }

        if (strlen($dql) > 0)
            $dql = (strpos($this->dql, "WHERE") === false) ? $this->dql." WHERE ".$dql : $this->dql." AND (".$dql.")";
        else
            $dql = $this->dql;

        if (strlen($order) > 0) $dql .= " ORDER BY ".$order;

        $query = $om->createQuery($dql);
        $items = $query->getResult();

        $data = $this->prepareTable($items, $request);

        return $data;
    }


    /**
     * Baue die gefilterte und sortierte Ergebnistabelle auf
     *
     * @param array $items
     * @return array
     */
    public function prepareTable(array $items, ?Request $request = null): array {
        $data = [];
        foreach ($items as $rowData) {
            $values = [];
            $cellClasses = null;
            foreach ($this->columns as $col) {
                $colName = $col->getName();
                $values[$colName] = $col->getItemValue($rowData);

                if (null !== $this->rowClassSetter) {
                    $cellClass = ($this->rowClassSetter)($rowData, $colName, $request);
                    if (null !== $cellClass) {
                        if (null === $cellClasses) $cellClasses = [];
                        $cellClasses[$colName] = $cellClass;
                    }
                }
            }

            $id = ('array' === gettype($rowData)) ? $rowData['id'] : $rowData->getId();

            $rowClass = null;
            if (null !== $this->rowClassSetter)
                $rowClass = ($this->rowClassSetter)($rowData, null, $request);

            $data[] = new ListRow($this, (null ===  $this->editroute) ? null : $this->editroute.$id, $values, $rowClass, $cellClasses);
        }

        return $data;
    }
}
